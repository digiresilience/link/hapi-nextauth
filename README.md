# hapi-nextauth

This is a plugin for hapi.js that exposes [NextAuth's database adapter](https://next-auth.js.org/tutorials/creating-a-database-adapter) via HTTP. Bring your own database.

## Usage

```typescript
import * as Hapi from "@hapi/hapi";
import * as Joi from "joi";
import NextAuthPlugin from "@digiresilience/hapi-nextauth";
import type { AdapterInstance } from "next-auth/adapters";


const server = new Hapi.Server();

// the validator must be registered before registering the plugin
await server.validator(Joi);

const nextAuthAdapterFactory: AdapterInstance = (request: Hapi.Request) => {
  ... instantiate your next auth adapter ...
}


// register the plugin
await server.register({
  plugin: NextAuthPlugin,
  options: {
    // the only required parameter is a function that returns your implementation of the NextAuthAdapter
    nextAuthAdapterFactory,
}});
```

Reference the [next-auth typings](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/next-auth/adapters.d.ts#L38-L77) for the adapter interface.

Options consist of:

- `nextAuthAdapterFactory` - a function that returns your implementation of the NextAuthAdapter, it takes the Hapi Request as the sole argument.
- `basePath` - a string that all next auth endpoints will be served from
- `sharedSecret` - the secret used for basic authentication to the nextauth endpoints
- `validators` - an object containing
  - `profile` - a Joi schema that validates a profile
  - `user` - a Joi schema that validates a user
  - `userId` - a Joi schema that validates a userId
  - `session` - a Joi schema that validates a session
- `tags` - tags to add to the endpoints

Defaults are defined in [`src/index.ts`](src/index.ts)

## Credits

Copyright © 2020-present [Center for Digital Resilience][cdr]

### Contributors

| [![Abel Luck][abelxluck_avatar]][abelxluck_homepage]<br/>[Abel Luck][abelxluck_homepage] |
| ---------------------------------------------------------------------------------------- |

[abelxluck_homepage]: https://gitlab.com/abelxluck
[abelxluck_avatar]: https://secure.gravatar.com/avatar/0f605397e0ead93a68e1be26dc26481a?s=100&d=identicon

### License

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.en.html)

    GNU AFFERO GENERAL PUBLIC LICENSE
    Version 3, 19 November 2007

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

[cdrtech]: https://digiresilience.org/tech/
[cdr]: https://digiresilience.org
