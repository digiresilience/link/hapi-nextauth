import type { AdapterInstance } from "next-auth/adapters";
import type { NumberSchema, StringSchema, ObjectSchema } from "joi";
import type { Request } from "@hapi/hapi";

export type AdapterFactory<TUser, TProfile, TSession> = (
  request: Request
) => AdapterInstance<TUser, TProfile, TSession>;

export interface NextAuthPluginOptions<TUser, TProfile, TSession> {
  nextAuthAdapterFactory: AdapterFactory<TUser, TProfile, TSession>;

  validators?: {
    profile: ObjectSchema;
    userId: StringSchema | NumberSchema;
    user: ObjectSchema;
    session: ObjectSchema;
  };
  sharedSecret?: string;
  basePath?: string;
  tags?: string[];
}
