# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.1](https://digiresilience.org/link/hapi-users/compare/0.2.0...0.2.1) (2021-10-08)

## [0.2.0](https://digiresilience.org/link/hapi-users/compare/0.1.0...0.2.0) (2021-05-03)


### ⚠ BREAKING CHANGES

* update deps

### Features

* update deps ([4fdf4d0](https://digiresilience.org/link/hapi-users/commit/4fdf4d0a2a25f76f1d3c27868145b0362e819195))

## [0.1.0](https://digiresilience.org/link/hapi-users/compare/0.0.3...0.1.0) (2021-04-30)


### ⚠ BREAKING CHANGES

* upgrade next-auth to 3.19.3

### Bug Fixes

* upgrade amigo-dev to 0.2.3 ([3fc9eaa](https://digiresilience.org/link/hapi-users/commit/3fc9eaa44658982887d6b8e6b6dc89044a7357de))
* upgrade next-auth to 3.19.3 ([cfb19b5](https://digiresilience.org/link/hapi-users/commit/cfb19b5ef43dd493fa1795c28b47c2d973f40132))

### [0.0.3](https://digiresilience.org/link/hapi-users/compare/0.0.2...0.0.3) (2020-11-24)


### Bug Fixes

* don't require package.json ([d9ca860](https://digiresilience.org/link/hapi-users/commit/d9ca860e8feeb46e9f19a1295313fe8f1efb45b5))

### [0.0.2](https://digiresilience.org/link/hapi-users/compare/0.0.1...0.0.2) (2020-11-24)


### Features

* do not register @hapi/basic, but declare a dependency on it ([8775d01](https://digiresilience.org/link/hapi-users/commit/8775d01778c42711d0b4aec15b0d25c0c7c040b8))
* implement basic auth for endpoint authorization ([0834f2e](https://digiresilience.org/link/hapi-users/commit/0834f2e9f2a618287767c18797b1ad7665b22bb1))

### 0.0.1 (2020-11-20)
