test: build
	mkdir -p coverage
	yarn test

.PHONY: build
build: node_modules/
	yarn build

lint:
	yarn test:lint

fmt:
	yarn fix:prettier
	yarn fix:lint

doc:
	yarn doc

publish: test
	npm publish

# always run yarn when we are in the CI
node_modules/: .npmrc
	@(test -d node_modules && test -z "${CI_JOB_TOKEN}") || yarn

.npmrc:
ifdef CI_JOB_TOKEN
	echo '@guardianproject-ops:registry=https://gitlab.com/api/v4/packages/npm/' > .npmrc
	echo '@digiresilience:registry=https://gitlab.com/api/v4/packages/npm/' > .npmrc
	echo '//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
endif

clean:
	rm -rf build tmp coverage yarn-error.log

distclean: clean
	rm -rf node_modules
